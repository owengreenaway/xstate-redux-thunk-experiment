import {ADD, UPDATE_QUERY} from "./actionTypes";
import { galleryMachine } from "./galleryMachine";
import { updateItems } from './reducers/gallery';
import fetchJsonp from "fetch-jsonp";

export const add = amount => ({
  type: ADD,
  payload: {
    amount
  }
});

export const updateQuery = query => ({
  type: UPDATE_QUERY,
  query
});

export const asyncStart = () => ({
  type: "ASYNC_START",
});

export const incrementAsync = () => (dispatch) => {
  dispatch(asyncStart());
  setTimeout(() => {
    // Yay! Can invoke sync or async actions with `dispatch`
    dispatch(add(3));
  }, 1000);
};

export const setCurrentView = view => ({
  type: "SET_CURRENT_VIEW",
  payload: {
    view
  }
});

export const updateStateMachine = (event) => (dispatch, getState) => {
  console.log("event", event);
  console.log("getState", getState());
  console.log("dispatch", dispatch);

  const state = getState()
  const currentGalleryState = state.currentView.currentStateMatchineState;
  const nextGalleryState =
    galleryMachine.transition(currentGalleryState, event.type);
  
  console.log("nextGalleryState", nextGalleryState)

  const nextState = nextGalleryState.actions
    .reduce((state, action) => command(action, event, dispatch) || state, undefined);

  dispatch(setCurrentView(nextGalleryState.value))
  // flipping.read();

  // this.setState({
  //   gallery: nextGalleryState.value,
  //   ...nextState
  // }, () => flipping.flip());
}

const command = (action, event, dispatch) => {
  console.log("command", action);
  switch (action) {
    case 'search':
      console.log('searcj')
      // execute the search command
      search(event.query, dispatch);
      break;
    case 'updateItems':
      console.log(event)
      if (event.items) {
        // update the state with the found items
        dispatch(updateItems(event.items))
      }
      break;
    case 'setPhoto':
      // if (event.item) {
      //   return { photo: event.item }
      // }
    default:
      break;
  }
}

const search = (query, dispatch) => {
  console.log('search f')
  const encodedQuery = encodeURIComponent(query);
  
  setTimeout(() => {      
    fetchJsonp(
      `https://api.flickr.com/services/feeds/photos_public.gne?lang=en-us&format=json&tags=${encodedQuery}`,
      { jsonpCallback: 'jsoncallback' })
      // .then(res => res.json())
      .then(data => {
        console.log('d', data);
        dispatch(updateStateMachine({ type: 'SEARCH_SUCCESS', items: mockData.items }));
      })
      .catch(error => {
        dispatch(updateStateMachine({ type: 'SEARCH_FAILURE' }));
      });
  }, 1000);
};

const mockData = {
  "title": "Recent Uploads tagged dog",
  "link": `https://www.flickr.com/photos/tags/dog/`,
  "description": "",
  "modified": "2018-10-19T14:46:45Z",
  "generator": "https:\/\/www.flickr.com",
  "items": [
    {
      "title": "Petes Famous Hot Dogs",
      "link": "https:\/\/www.flickr.com\/photos\/154004763@N02\/44704803874\/",
      "media": {"m":"https:\/\/farm2.staticflickr.com\/1968\/44704803874_eaa4680051_m.jpg"},
      "date_taken": "2015-11-30T10:09:10-08:00",
      "description": " <p><a href=\"https:\/\/www.flickr.com\/people\/154004763@N02\/\">dwheel41<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/154004763@N02\/44704803874\/\" title=\"Petes Famous Hot Dogs\"><img src=\"https:\/\/farm2.staticflickr.com\/1968\/44704803874_eaa4680051_m.jpg\" width=\"240\" height=\"180\" alt=\"Petes Famous Hot Dogs\" \/><\/a><\/p> <p>SI Exif<\/p>",
      "published": "2018-10-19T14:46:45Z",
      "author": "nobody@flickr.com (\"dwheel41\")",
      "author_id": "154004763@N02",
      "tags": "sign hot dog reflection petes famous dogs window"
    },
    {
      "title": "2018-10-13 We Two and a Dog",
      "link": "https:\/\/www.flickr.com\/photos\/26181971@N07\/43611890670\/",
      "media": {"m":"https:\/\/farm2.staticflickr.com\/1972\/43611890670_d3f2815f39_m.jpg"},
      "date_taken": "2018-10-13T10:51:32-08:00",
      "description": " <p><a href=\"https:\/\/www.flickr.com\/people\/26181971@N07\/\">beranekp<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/26181971@N07\/43611890670\/\" title=\"2018-10-13 We Two and a Dog\"><img src=\"https:\/\/farm2.staticflickr.com\/1972\/43611890670_d3f2815f39_m.jpg\" width=\"240\" height=\"160\" alt=\"2018-10-13 We Two and a Dog\" \/><\/a><\/p> <p>Czech Republic - We Two and a Dog in Teplice<\/p>",
      "published": "2018-10-19T14:49:40Z",
      "author": "nobody@flickr.com (\"beranekp\")",
      "author_id": "26181971@N07",
      "tags": "czech teplice teplitz people dog"
    },
    {
      "title": "Gettysburg, October 2018",
      "link": "https:\/\/www.flickr.com\/photos\/100859345@N05\/31553697668\/",
      "media": {"m":"https:\/\/farm2.staticflickr.com\/1914\/31553697668_be098caa89_m.jpg"},
      "date_taken": "2018-10-18T08:29:45-08:00",
      "description": " <p><a href=\"https:\/\/www.flickr.com\/people\/100859345@N05\/\">bpephin<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/100859345@N05\/31553697668\/\" title=\"Gettysburg, October 2018\"><img src=\"https:\/\/farm2.staticflickr.com\/1914\/31553697668_be098caa89_m.jpg\" width=\"117\" height=\"240\" alt=\"Gettysburg, October 2018\" \/><\/a><\/p> <p>Irish wolfhound at the Irish Brigade Monument<\/p>",
      "published": "2018-10-19T14:46:03Z",
      "author": "nobody@flickr.com (\"bpephin\")",
      "author_id": "100859345@N05",
      "tags": "irish wolfhound gettysburg civilwar dog statue monument brigade"
    },
    {
      "title": "Jr. Sunrise",
      "link": "https:\/\/www.flickr.com\/photos\/155666059@N04\/31553185088\/",
      "media": {"m":"https:\/\/farm2.staticflickr.com\/1962\/31553185088_339816404d_m.jpg"},
      "date_taken": "2018-10-18T06:05:38-08:00",
      "description": " <p><a href=\"https:\/\/www.flickr.com\/people\/155666059@N04\/\">progreentalent<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/155666059@N04\/31553185088\/\" title=\"Jr. Sunrise\"><img src=\"https:\/\/farm2.staticflickr.com\/1962\/31553185088_339816404d_m.jpg\" width=\"135\" height=\"240\" alt=\"Jr. Sunrise\" \/><\/a><\/p> <p><\/p>",
      "published": "2018-10-19T14:15:42Z",
      "author": "nobody@flickr.com (\"progreentalent\")",
      "author_id": "155666059@N04",
      "tags": "huskee dog pet sunrise beautiful"
    },
    {
      "title": "Jr. Sunrise",
      "link": "https:\/\/www.flickr.com\/photos\/155666059@N04\/31553199228\/",
      "media": {"m":"https:\/\/farm2.staticflickr.com\/1934\/31553199228_88a90b687d_m.jpg"},
      "date_taken": "2018-10-18T05:54:29-08:00",
      "description": " <p><a href=\"https:\/\/www.flickr.com\/people\/155666059@N04\/\">progreentalent<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/155666059@N04\/31553199228\/\" title=\"Jr. Sunrise\"><img src=\"https:\/\/farm2.staticflickr.com\/1934\/31553199228_88a90b687d_m.jpg\" width=\"240\" height=\"135\" alt=\"Jr. Sunrise\" \/><\/a><\/p> <p><\/p>",
      "published": "2018-10-19T14:15:34Z",
      "author": "nobody@flickr.com (\"progreentalent\")",
      "author_id": "155666059@N04",
      "tags": "huskee dog pet sunrise beautiful"
    },
    {
      "title": "Jr. Sunrise",
      "link": "https:\/\/www.flickr.com\/photos\/155666059@N04\/31553197938\/",
      "media": {"m":"https:\/\/farm2.staticflickr.com\/1964\/31553197938_ca7f3aa112_m.jpg"},
      "date_taken": "2018-10-18T05:55:12-08:00",
      "description": " <p><a href=\"https:\/\/www.flickr.com\/people\/155666059@N04\/\">progreentalent<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/155666059@N04\/31553197938\/\" title=\"Jr. Sunrise\"><img src=\"https:\/\/farm2.staticflickr.com\/1964\/31553197938_ca7f3aa112_m.jpg\" width=\"240\" height=\"135\" alt=\"Jr. Sunrise\" \/><\/a><\/p> <p><\/p>",
      "published": "2018-10-19T14:15:35Z",
      "author": "nobody@flickr.com (\"progreentalent\")",
      "author_id": "155666059@N04",
      "tags": "huskee dog pet sunrise beautiful"
    },
    {
      "title": "Jr. Sunrise",
      "link": "https:\/\/www.flickr.com\/photos\/155666059@N04\/31553182968\/",
      "media": {"m":"https:\/\/farm2.staticflickr.com\/1925\/31553182968_52615443c2_m.jpg"},
      "date_taken": "2018-10-18T06:06:39-08:00",
      "description": " <p><a href=\"https:\/\/www.flickr.com\/people\/155666059@N04\/\">progreentalent<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/155666059@N04\/31553182968\/\" title=\"Jr. Sunrise\"><img src=\"https:\/\/farm2.staticflickr.com\/1925\/31553182968_52615443c2_m.jpg\" width=\"240\" height=\"135\" alt=\"Jr. Sunrise\" \/><\/a><\/p> <p><\/p>",
      "published": "2018-10-19T14:15:43Z",
      "author": "nobody@flickr.com (\"progreentalent\")",
      "author_id": "155666059@N04",
      "tags": "huskee dog pet sunrise beautiful"
    },
    {
      "title": "Jr. Sunrise",
      "link": "https:\/\/www.flickr.com\/photos\/155666059@N04\/43611239970\/",
      "media": {"m":"https:\/\/farm2.staticflickr.com\/1973\/43611239970_1f9ec93484_m.jpg"},
      "date_taken": "2018-10-18T06:05:05-08:00",
      "description": " <p><a href=\"https:\/\/www.flickr.com\/people\/155666059@N04\/\">progreentalent<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/155666059@N04\/43611239970\/\" title=\"Jr. Sunrise\"><img src=\"https:\/\/farm2.staticflickr.com\/1973\/43611239970_1f9ec93484_m.jpg\" width=\"240\" height=\"135\" alt=\"Jr. Sunrise\" \/><\/a><\/p> <p><\/p>",
      "published": "2018-10-19T14:15:40Z",
      "author": "nobody@flickr.com (\"progreentalent\")",
      "author_id": "155666059@N04",
      "tags": "huskee dog pet sunrise beautiful"
    },
    {
      "title": "Jr. Sunrise",
      "link": "https:\/\/www.flickr.com\/photos\/155666059@N04\/43611252180\/",
      "media": {"m":"https:\/\/farm2.staticflickr.com\/1901\/43611252180_42bc9a88df_m.jpg"},
      "date_taken": "2018-10-18T05:54:17-08:00",
      "description": " <p><a href=\"https:\/\/www.flickr.com\/people\/155666059@N04\/\">progreentalent<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/155666059@N04\/43611252180\/\" title=\"Jr. Sunrise\"><img src=\"https:\/\/farm2.staticflickr.com\/1901\/43611252180_42bc9a88df_m.jpg\" width=\"240\" height=\"135\" alt=\"Jr. Sunrise\" \/><\/a><\/p> <p><\/p>",
      "published": "2018-10-19T14:15:33Z",
      "author": "nobody@flickr.com (\"progreentalent\")",
      "author_id": "155666059@N04",
      "tags": "huskee dog pet sunrise beautiful"
    },
    {
      "title": "Jr. Sunrise",
      "link": "https:\/\/www.flickr.com\/photos\/155666059@N04\/45428706371\/",
      "media": {"m":"https:\/\/farm2.staticflickr.com\/1957\/45428706371_2817d97289_m.jpg"},
      "date_taken": "2018-10-18T06:08:33-08:00",
      "description": " <p><a href=\"https:\/\/www.flickr.com\/people\/155666059@N04\/\">progreentalent<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/155666059@N04\/45428706371\/\" title=\"Jr. Sunrise\"><img src=\"https:\/\/farm2.staticflickr.com\/1957\/45428706371_2817d97289_m.jpg\" width=\"240\" height=\"135\" alt=\"Jr. Sunrise\" \/><\/a><\/p> <p><\/p>",
      "published": "2018-10-19T14:15:46Z",
      "author": "nobody@flickr.com (\"progreentalent\")",
      "author_id": "155666059@N04",
      "tags": "huskee dog pet sunrise beautiful"
    },
    {
      "title": "Jr. Sunrise",
      "link": "https:\/\/www.flickr.com\/photos\/155666059@N04\/31553189848\/",
      "media": {"m":"https:\/\/farm2.staticflickr.com\/1925\/31553189848_d6bcb24c49_m.jpg"},
      "date_taken": "2018-10-18T06:05:00-08:00",
      "description": " <p><a href=\"https:\/\/www.flickr.com\/people\/155666059@N04\/\">progreentalent<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/155666059@N04\/31553189848\/\" title=\"Jr. Sunrise\"><img src=\"https:\/\/farm2.staticflickr.com\/1925\/31553189848_d6bcb24c49_m.jpg\" width=\"240\" height=\"135\" alt=\"Jr. Sunrise\" \/><\/a><\/p> <p><\/p>",
      "published": "2018-10-19T14:15:40Z",
      "author": "nobody@flickr.com (\"progreentalent\")",
      "author_id": "155666059@N04",
      "tags": "huskee dog pet sunrise beautiful"
    },
    {
      "title": "Jr. Sunrise",
      "link": "https:\/\/www.flickr.com\/photos\/155666059@N04\/31553194378\/",
      "media": {"m":"https:\/\/farm2.staticflickr.com\/1949\/31553194378_dd1c307a41_m.jpg"},
      "date_taken": "2018-10-18T05:55:20-08:00",
      "description": " <p><a href=\"https:\/\/www.flickr.com\/people\/155666059@N04\/\">progreentalent<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/155666059@N04\/31553194378\/\" title=\"Jr. Sunrise\"><img src=\"https:\/\/farm2.staticflickr.com\/1949\/31553194378_dd1c307a41_m.jpg\" width=\"240\" height=\"135\" alt=\"Jr. Sunrise\" \/><\/a><\/p> <p><\/p>",
      "published": "2018-10-19T14:15:37Z",
      "author": "nobody@flickr.com (\"progreentalent\")",
      "author_id": "155666059@N04",
      "tags": "huskee dog pet sunrise beautiful"
    },
    {
      "title": "Jr. Sunrise",
      "link": "https:\/\/www.flickr.com\/photos\/155666059@N04\/31553177278\/",
      "media": {"m":"https:\/\/farm2.staticflickr.com\/1944\/31553177278_fb9330a9c4_m.jpg"},
      "date_taken": "2018-10-18T06:07:14-08:00",
      "description": " <p><a href=\"https:\/\/www.flickr.com\/people\/155666059@N04\/\">progreentalent<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/155666059@N04\/31553177278\/\" title=\"Jr. Sunrise\"><img src=\"https:\/\/farm2.staticflickr.com\/1944\/31553177278_fb9330a9c4_m.jpg\" width=\"135\" height=\"240\" alt=\"Jr. Sunrise\" \/><\/a><\/p> <p><\/p>",
      "published": "2018-10-19T14:15:45Z",
      "author": "nobody@flickr.com (\"progreentalent\")",
      "author_id": "155666059@N04",
      "tags": "huskee dog pet sunrise beautiful"
    },
    {
      "title": "Jr. Sunrise",
      "link": "https:\/\/www.flickr.com\/photos\/155666059@N04\/31553191448\/",
      "media": {"m":"https:\/\/farm2.staticflickr.com\/1920\/31553191448_689d060792_m.jpg"},
      "date_taken": "2018-10-18T05:55:44-08:00",
      "description": " <p><a href=\"https:\/\/www.flickr.com\/people\/155666059@N04\/\">progreentalent<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/155666059@N04\/31553191448\/\" title=\"Jr. Sunrise\"><img src=\"https:\/\/farm2.staticflickr.com\/1920\/31553191448_689d060792_m.jpg\" width=\"240\" height=\"135\" alt=\"Jr. Sunrise\" \/><\/a><\/p> <p><\/p>",
      "published": "2018-10-19T14:15:39Z",
      "author": "nobody@flickr.com (\"progreentalent\")",
      "author_id": "155666059@N04",
      "tags": "huskee dog pet sunrise beautiful"
    },
    {
      "title": "Jr. Sunrise",
      "link": "https:\/\/www.flickr.com\/photos\/155666059@N04\/45428709471\/",
      "media": {"m":"https:\/\/farm2.staticflickr.com\/1936\/45428709471_bfa9982fe4_m.jpg"},
      "date_taken": "2018-10-18T06:07:19-08:00",
      "description": " <p><a href=\"https:\/\/www.flickr.com\/people\/155666059@N04\/\">progreentalent<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/155666059@N04\/45428709471\/\" title=\"Jr. Sunrise\"><img src=\"https:\/\/farm2.staticflickr.com\/1936\/45428709471_bfa9982fe4_m.jpg\" width=\"135\" height=\"240\" alt=\"Jr. Sunrise\" \/><\/a><\/p> <p><\/p>",
      "published": "2018-10-19T14:15:45Z",
      "author": "nobody@flickr.com (\"progreentalent\")",
      "author_id": "155666059@N04",
      "tags": "huskee dog pet sunrise beautiful"
    },
    {
      "title": "Jr. Sunrise",
      "link": "https:\/\/www.flickr.com\/photos\/155666059@N04\/45376873512\/",
      "media": {"m":"https:\/\/farm2.staticflickr.com\/1927\/45376873512_0610dfae07_m.jpg"},
      "date_taken": "2018-10-18T06:22:02-08:00",
      "description": " <p><a href=\"https:\/\/www.flickr.com\/people\/155666059@N04\/\">progreentalent<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/155666059@N04\/45376873512\/\" title=\"Jr. Sunrise\"><img src=\"https:\/\/farm2.staticflickr.com\/1927\/45376873512_0610dfae07_m.jpg\" width=\"240\" height=\"135\" alt=\"Jr. Sunrise\" \/><\/a><\/p> <p><\/p>",
      "published": "2018-10-19T14:15:49Z",
      "author": "nobody@flickr.com (\"progreentalent\")",
      "author_id": "155666059@N04",
      "tags": "huskee dog pet sunrise beautiful"
    },
    {
      "title": "Jr. Sunrise",
      "link": "https:\/\/www.flickr.com\/photos\/155666059@N04\/31553187268\/",
      "media": {"m":"https:\/\/farm2.staticflickr.com\/1933\/31553187268_eff4081b94_m.jpg"},
      "date_taken": "2018-10-18T06:05:05-08:00",
      "description": " <p><a href=\"https:\/\/www.flickr.com\/people\/155666059@N04\/\">progreentalent<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/155666059@N04\/31553187268\/\" title=\"Jr. Sunrise\"><img src=\"https:\/\/farm2.staticflickr.com\/1933\/31553187268_eff4081b94_m.jpg\" width=\"240\" height=\"135\" alt=\"Jr. Sunrise\" \/><\/a><\/p> <p><\/p>",
      "published": "2018-10-19T14:15:41Z",
      "author": "nobody@flickr.com (\"progreentalent\")",
      "author_id": "155666059@N04",
      "tags": "huskee dog pet sunrise beautiful"
    },
    {
      "title": "Jr. Sunrise",
      "link": "https:\/\/www.flickr.com\/photos\/155666059@N04\/44704327174\/",
      "media": {"m":"https:\/\/farm2.staticflickr.com\/1954\/44704327174_7e3b4cc5d0_m.jpg"},
      "date_taken": "2018-10-18T06:08:49-08:00",
      "description": " <p><a href=\"https:\/\/www.flickr.com\/people\/155666059@N04\/\">progreentalent<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/155666059@N04\/44704327174\/\" title=\"Jr. Sunrise\"><img src=\"https:\/\/farm2.staticflickr.com\/1954\/44704327174_7e3b4cc5d0_m.jpg\" width=\"135\" height=\"240\" alt=\"Jr. Sunrise\" \/><\/a><\/p> <p><\/p>",
      "published": "2018-10-19T14:15:47Z",
      "author": "nobody@flickr.com (\"progreentalent\")",
      "author_id": "155666059@N04",
      "tags": "huskee dog pet sunrise beautiful"
    },
    {
      "title": "Jr. Sunrise",
      "link": "https:\/\/www.flickr.com\/photos\/155666059@N04\/31553169658\/",
      "media": {"m":"https:\/\/farm2.staticflickr.com\/1969\/31553169658_39cd155ea9_m.jpg"},
      "date_taken": "2018-10-18T06:14:18-08:00",
      "description": " <p><a href=\"https:\/\/www.flickr.com\/people\/155666059@N04\/\">progreentalent<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/155666059@N04\/31553169658\/\" title=\"Jr. Sunrise\"><img src=\"https:\/\/farm2.staticflickr.com\/1969\/31553169658_39cd155ea9_m.jpg\" width=\"240\" height=\"135\" alt=\"Jr. Sunrise\" \/><\/a><\/p> <p><\/p>",
      "published": "2018-10-19T14:15:48Z",
      "author": "nobody@flickr.com (\"progreentalent\")",
      "author_id": "155666059@N04",
      "tags": "huskee dog pet sunrise beautiful"
    },
    {
      "title": "Jr. Sunrise",
      "link": "https:\/\/www.flickr.com\/photos\/155666059@N04\/30488064457\/",
      "media": {"m":"https:\/\/farm2.staticflickr.com\/1940\/30488064457_fba4dfc42d_m.jpg"},
      "date_taken": "2018-10-18T06:06:50-08:00",
      "description": " <p><a href=\"https:\/\/www.flickr.com\/people\/155666059@N04\/\">progreentalent<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/155666059@N04\/30488064457\/\" title=\"Jr. Sunrise\"><img src=\"https:\/\/farm2.staticflickr.com\/1940\/30488064457_fba4dfc42d_m.jpg\" width=\"240\" height=\"135\" alt=\"Jr. Sunrise\" \/><\/a><\/p> <p><\/p>",
      "published": "2018-10-19T14:15:44Z",
      "author": "nobody@flickr.com (\"progreentalent\")",
      "author_id": "155666059@N04",
      "tags": "huskee dog pet sunrise beautiful"
    }
  ]
};