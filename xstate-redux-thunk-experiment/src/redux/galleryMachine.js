import { Machine } from 'xstate';

export const galleryMachine = Machine({
  initial: 'start',
  states: {
    start: {
      on: {
        SEARCH: 'loading'
      }
    },
    loading: {
      onEntry: ['search'],
      on: {
        SEARCH_SUCCESS: 'gallery',
        // SEARCH_SUCCESS: {
        //   target: 'gallery',
        //   actions: ['updateItems']
        // },
        SEARCH_FAILURE: 'error',
        CANCEL_SEARCH: 'gallery'
      }
    },
    error: {
      on: {
        SEARCH: 'loading'
      }
    },
    gallery: {
      onEntry: ['updateItems'],
      on: {
        SEARCH: 'loading',
        SELECT_PHOTO: 'photo'
      }
    },
    photo: {
      onEntry: ['setPhoto'],
      on: {
        EXIT_PHOTO: 'gallery'
      }
    }
  }
});
