import {galleryMachine} from "../galleryMachine";

const initialState = {
  currentStateMatchineState: galleryMachine.initialState.value
};

export default function(state = initialState, action) {
  switch (action.type) {
    case "SET_CURRENT_VIEW": {
      return {
        currentStateMatchineState: action.payload.view
      };
    }
    default:
      return state;
  }
}
