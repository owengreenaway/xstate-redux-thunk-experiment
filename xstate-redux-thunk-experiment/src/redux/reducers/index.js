import { combineReducers } from "redux";
import counter from "./counter";
import currentView from "./currentView";
import gallery from "./gallery";

export default combineReducers({ counter, currentView, gallery });
