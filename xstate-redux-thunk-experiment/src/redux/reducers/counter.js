import { ADD } from "../actionTypes";

const initialState = {
  counterValue: 10
};

export default function(state = initialState, action) {
  switch (action.type) {
    case ADD: {
      const { amount } = action.payload;
      return {
        counterValue: state.counterValue + amount
      };
    }
    default:
      return state;
  }
}
