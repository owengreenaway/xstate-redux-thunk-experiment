import { UPDATE_QUERY, UPDATE_ITEMS } from "../actionTypes";

const initialState = {
  query: "",
  items: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case UPDATE_QUERY: {
      return {
        query: action.query,
        items: state.items
      };
    }
    case UPDATE_ITEMS: {
      return {
        query: state.query,
        items: action.items
      };
    }
    default:
      return state;
  }
}

export const updateItems = items => ({
  type: "UPDATE_ITEMS",
  items
});