import React from "react";
import { connect } from "react-redux";
import Form from "./Form";

const Router = ({ currentView, items }) => {
  if (currentView === 'start') {
    return (
      <div className="ui-app" data-state={'start'}>
        <Form/>
      </div>
    );
  }

  if (currentView === 'error') {
    return (
      <div className="ui-app" data-state={'error'}>
        <Form/>
        <section className="ui-items" data-state={'error'}>
          <span className="ui-error">Uh oh, search failed.</span>
        </section>
      </div>
    );
  }

  return (
    <div className="ui-app" data-state={currentView}>
      <Form/>

      <section className="ui-items" data-state={currentView}>
        {items.map((item, i) =>
          <img
            src={item.media.m}
            className="ui-item"
            style={{'--i': i}}
            key={item.link}
            // onClick={() => this.send({
            //   type: 'SELECT_PHOTO', item
            // })}
            onClick={() => {
            }}
          />
        )
        }
      </section>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    currentView: state.currentView.currentStateMatchineState,
    items: state.gallery.items
  };
};

export default connect(
  mapStateToProps,
  {}
)(Router);
