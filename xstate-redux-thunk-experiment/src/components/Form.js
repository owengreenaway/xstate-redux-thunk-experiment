import React from "react";
import { connect } from "react-redux";
import {updateQuery, updateStateMachine} from "../redux/actions";

const Form = ({ state, query, updateQuery, updateStateMachine }) => {
  const searchText = {
    loading: 'Searching...',
    error: 'Try search again',
    start: 'Search'
  }[state] || 'Search';

  const handleSubmit = (e) => {
    e.preventDefault();
    updateStateMachine({ type: 'SEARCH', query })
    // this.send({ type: 'SEARCH', query: this.state.query });
  }

  return (
    <form className="ui-form" onSubmit={handleSubmit}>
      <input
        type="search"
        className="ui-input"
        value={query}
        onChange={e => {
          console.log('hit');
          updateQuery(e.target.value)
        }}
        placeholder="Search Flickr for photos..."
        disabled={state === 'loading'}
      />
      <div className="ui-buttons">
        <button
          className="ui-button"
          disabled={state === 'loading'}
          data-flip-key="search">
          {searchText}
        </button>
        {state === 'loading' &&
        <button
          className="ui-button"
          type="button"
          onClick={() => updateStateMachine({ type: 'CANCEL_SEARCH' })}
        >
          Cancel
        </button>
        }
      </div>
    </form>
  )
};

const mapStateToProps = state => {
  return {
    state: state.currentView.currentStateMatchineState,
    query: state.gallery.query
  };
};
export default connect(
  mapStateToProps,
  {updateQuery, updateStateMachine}
)(Form);
