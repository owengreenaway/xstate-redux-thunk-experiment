import React from "react";
import { connect } from "react-redux";
import { add, incrementAsync } from "../redux/actions";

const Example = ({ counter, add, incrementAsync }) => {
  const onClickHandler = () => add(2);
  return (
    <div>
      <p>Counter: {counter}</p>
      <button onClick={onClickHandler}>Add 2</button>
      <button onClick={incrementAsync}>Add 3 async</button>
    </div>
  );
};

const mapStateToProps = state => {
  return { counter: state.counterValue };
};
export default connect(
  mapStateToProps,
  {add, incrementAsync}
)(Example);
