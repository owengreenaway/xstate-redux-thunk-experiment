import React, { Component } from 'react';
// import Example from "./components/Example";
import Router from "./components/Router";
import './App.css';

class App extends Component {
  render() {
    return <Router />;
    // return (
    //   <div>
    //     <Example />
    //     <Router />
    //   </div>
    // );
  }
}

export default App;
