# xstate-redux-thunk-experiment

## Introduction

This is an experiment to build an app using redux, redux-thunk and a state machine using xstate. This is a rebuild of David's gallery example.

## Links

http://davidkpiano.github.io/xstate/docs/#/examples/gallery
https://codepen.io/davidkpiano/pen/dJJMWE

## Disclaimer

This is only an experiment so doesn't contain tests nor typings.